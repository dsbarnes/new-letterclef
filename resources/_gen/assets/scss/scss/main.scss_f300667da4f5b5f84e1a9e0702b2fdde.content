* {
  margin: 0;
  padding: 0;
  border: 0;
  box-sizing: border-box;
  text-decoration: none;
  list-style-type: none;
  font-style: normal;
  font-size: 1em; }

h1, h2, h3, h4 {
  font-family: "Raleway", sans-serif;
  font-weight: 400; }

#nav-header {
  font-size: 24pt; }

h1 {
  font-size: clamp(32pt, 6vw, 64pt); }

h2 {
  font-size: clamp(24pt, 4vw, 40pt); }

p {
  font-family: "Times New Roman", serif;
  font-size: 14pt;
  max-width: clamp(498px, 64%, 640px); }

small {
  font-size: 12pt; }

a {
  color: #525252; }

figure {
  margin: 2rem 0; }
  figure picture img {
    width: clamp(398px, 80%, 800px); }
  figure figcaption {
    width: clamp(398px, 80%, 800px);
    position: relative;
    bottom: .25rem; }
    figure figcaption small {
      display: block;
      padding: .25rem; }

nav {
  display: flex;
  justify-content: space-between;
  margin-left: clamp(8px, 2%, 80px);
  padding: 1rem; }
  nav h1#nav-header a {
    color: #000000; }
    nav h1#nav-header a:hover {
      background-color: yellow; }
  nav ul {
    display: flex;
    justify-content: space-between; }
    nav ul li {
      padding: 8px;
      font-family: "Times New Roman", serif;
      font-size: 14pt; }
      nav ul li a:hover {
        border-bottom: solid 1.5em yellow; }
  nav .active-nav-item {
    background-color: yellow; }

hr#header-rule {
  margin-bottom: 4rem; }

section#toc {
  background-color: #F5F5F5;
  margin: 1rem 0 2rem 0;
  max-width: 50%;
  min-width: 398px;
  border: 1px solid black; }
  section#toc p {
    padding: 1ch 0; }
  section#toc hr {
    margin-bottom: 0.5rem; }

nav#TableOfContents {
  margin: 0;
  padding: 0; }
  nav#TableOfContents ul {
    display: block; }
    nav#TableOfContents ul li {
      padding: 1ch 3ch; }
      nav#TableOfContents ul li a:hover {
        border-bottom: none;
        background-color: yellow; }

a#skip-nav {
  position: absolute;
  left: -999px;
  z-index: -999; }

a#skip-nav:focus, a#skip-nav:active {
  left: auto;
  top: auto;
  height: auto;
  overflow: auto;
  z-index: 999; }

hr {
  border-top: 1px solid #000000; }

body {
  max-width: 80%; }

main {
  margin-left: clamp(8px, 12vw, 160px); }

main.list h1 {
  margin-bottom: 4rem; }

main.list ul li {
  margin: 0 0 8rem 0; }
  main.list ul li h1 {
    margin: 0 0 2rem 0;
    max-width: clamp(398px, 100%, 825px); }
    main.list ul li h1 a:hover {
      border-bottom: solid 1.5em yellow; }
  main.list ul li #article-description {
    margin: 2rem 0; }
  main.list ul li small a:hover {
    border-bottom: solid 1.5em yellow; }

main.list #taxonomies a {
  text-decoration: underline; }

main.terms h1 {
  margin-bottom: 4rem; }

main.terms ul li {
  margin-bottom: 4rem; }
  main.terms ul li h1 {
    margin: 0; }

main.single h1 {
  max-width: clamp(398px, 100%, 825px); }
  main.single h1 a:hover {
    border-bottom: yellow; }

main.single h1, main.single h2, main.single h3 {
  margin: 6rem 0 2rem 0; }

main.single h1:first-of-type, main.single h1 + p, main.single h2 + p, main.single h3 + p {
  margin-top: 0; }

main.single p {
  margin: 1rem 0; }

main.single p:first-of-type {
  margin: 0; }

main.single nav a {
  text-decoration: none; }

main.single a {
  text-decoration: underline; }

main.error code {
  color: #525252;
  display: block;
  max-width: clamp(498px, 64%, 640px); }

footer {
  margin-top: 240px; }
  footer section {
    display: flex;
    justify-content: space-around;
    margin: 0 auto;
    max-width: 80%; }
    footer section i {
      margin: 1rem; }
  footer small {
    display: block;
    margin: 1rem; }

/* Small devices */
/* Medium devices */
@media only screen and (max-width: 945px) {
  nav {
    flex-direction: column;
    align-items: flex-start;
    margin-left: clamp(8px, 2%, 80px);
    padding: 1rem; }
    nav h1#nav-header a {
      color: #000000; }
      nav h1#nav-header a:hover {
        background-color: yellow; }
    nav ul {
      margin-top: 1rem;
      display: flex;
      justify-content: space-between; }
      nav ul li {
        padding: 8px 16px 8px 0px;
        font-family: "Times New Roman", serif;
        font-size: 14pt; }
        nav ul li a:hover {
          border-bottom: solid 1.5em yellow; } }

/* Large devices */
/* Freeze */
