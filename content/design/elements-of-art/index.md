---
title: The Seven Elements of Art
subtitle:
date: 2022-01-04T12:35:30-05:00
description: The 7 elements of art are the building blocks for all things "Art." This article investigates and provides visual examples for each.

draft: false 
robotsdisallow: false

categories: [ art ]
tags: []
series: []

video: 
audio: 

image: /design/elements-of-art/img/Cover.png
imgalt: 
figcaption: 
---
![Poster](img/Elements-of-Art-Poster.png)
![Form](img/Form.png)
![Hut](img/Hue.png)
![Line](img/Line.png)
![Shape](img/Shape.png)
![Space](img/Space.png)
![Texture](img/Texture.png)
![Value](img/Value.png)