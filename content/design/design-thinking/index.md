---
title: Design Thinking
subtitle: There's some things money can't buy. For everything else, there's design thinking.
date: 2022-04-05T12:34:17-05:00
description: Some quick takes on Design Thinking and a simple poster set.
draft: false
robotsdisallow: false

categories: [ designg ]
tags: []
series: [ fundamentals ]

video:
audio:

image: /design/design-thinking/img/Cover.png
imgalt: Physical light bulbs hanging over a designer as they work.
figcaption: 
---

Perhaps Tim Brown coined the term "design thinking," and perhaps David M. Kelley adapted tools and models of design to solve business problems.  

Of course people will tell you IDEO invented design thinking (unless if you ask them).  

The story of who what when and where, although fun, is not the concern of design thinking itself.  

What matters is that design thinking, very likely, is the most important set of philosophies and tools ever created in the history of design.  

Air BNB famously implemented design thinking processes and turned their $200 USD per week business into the million dollar behemoth it is today.  

Burberry (love), once written off as a failed brand, used design thinking to embrace social media channels and, in a sense, save themselves from the internet.   

The former CEO of IBM, Thomas Watson Jr. famously said, “Good design is good business.”  

Agreed.  

A Design sprint usually consists of a 4 or 5 day process, stating with inspiration, mood boards, mapping, and sketching, and ending with a functioning, tested prototype.  

Bank of Americas "keep the change" program was conceived through design thinking.  

You might say, there's some things money can't buy.  
For ever thing else, theres ... design thinking.  

Design thinking is extremely flexible.  
Design thinking is a philosophy, and a tool set.  
Design thinking is a process.  
Design thinking is **empathy**.  

The design sprint is a 4-5 day all out attack on a problem, in search of a solution.  

It is common the sprint to consist of fully defining and understanding the problem, mapping and sketching, decision making, prototyping and testing. The goal isn't to have a final solution, it's to have a meaningful, progressive solution.  

Crazy solutions are good too, remember that guy Mendeleev who discoverd the periodicity of the elements, and then to prove it discoverd silicon in his head? When someone else claimed to discover silicon he wrote them a letter that said something to the effect of "Wrong - do this test and then you'll see, oh by the way this ones mine, you'll know I came up with it once you do the test, also, I discovered periodicity and that's my test, no other physical testing has occurred on my end but, it can't be any other way... Please do the test and prove me right twice. Two proofs one stone." 

Unlike the [elements of art](/art/elements-of-art.md ) or the
[principles of design](/design/principles-of-design.md),
design thinking has ordered steps. Each builds on the previous.  

![Empathize](img/Empathize.png)
![Define](img/Define.png)
![Ideate](img/Ideate.png)
![Prototype](img/Prototype.png)
![Test](img/Test.png)