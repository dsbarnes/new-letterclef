---
title: The 7 Gestalt Principles of Composition
subtitle:
date: 2021-12-06
description: Twitter Card and OG Text
draft: false
robotsdisallow: false

categories: [ design, history ]
tags: []
series:  []

video: 
audio: 

image: /design/gestalt-principles/img/Cover.png
imgalt:
figcaption: 
---

![Poster](img/Gestalt-Poster.png)
![Closure](img/Closure.png)
![Continuity](img/Continuity.png)
![Figure-Ground](img/Figure-Ground.png)
![Grouping](img/Grouping.png)
![Similarity](Similarity.png)
![Symmetry](img/Symmetry.png)
![Synchrony](img/Synchrony.png)