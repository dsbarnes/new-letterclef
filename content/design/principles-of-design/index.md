---
title: The 12 Principles of Design
subtitle:
date: 2021-12-06
description: The elements and principles of art and design make up the foundations for creating visually appealing compositions and communication. The elements are the foundational concepts of art and design. The principles are applications of the elements. This article defines and provides examples for each.
draft: false
robotsdisallow: false

categories: [design]
tags: []
series:  []

video: 
audio: 

image: /design/principles-of-design/img/Cover.png
imgalt:
figcaption:
---

![Poster](img/Principles-of-Design-Poster.png)
![Contrast](img/Contrast.png)
![Emphasis](img/Emphasis.png)
![Hierarchy](img/Hierarchy.png)
![Movement](img/Movement.png)
![Proportion](img/Proportion.png)
![Rhythm](img/Rhythm.png)
![Variety](img/Variety.png)
